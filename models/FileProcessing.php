<?php // Stan 2013-09-17

namespace app\models;


class FileProcessing extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'fileprocessings';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getFile()
  {
    return $this->hasOne('File', array('id' => '_files_id'));
  }

  public function getHandler()
  {
    return $this->hasOne('Handler', array('id' => '_handlers_id'));
  }

  public function getSheets()
  {
    return $this->hasMany('Sheet', array('_fileprocessings_id' => 'id'));
  }


}
?>
