<?php // Stan 2013-09-17

namespace app\models;


class Sheet extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'sheets';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getFile()
  {
    return $this->hasOne('File', array('id' => '_files_id'));
  }

  public function getFileProcessing()
  {
    return $this->hasOne('FileProcessing', array('id' => '_fileprocessings_id'));
  }

  public function getDocs()
  {
    return $this->hasMany('Doc', array('_sheets_id' => 'id'));
  }


}
?>
