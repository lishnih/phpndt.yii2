<?php // Stan 2013-05-24

namespace app\models;


class Joint extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'joints';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getJointEntries()
  {
    return $this->hasMany('JointEntry', array('_joints_id' => 'id'));
  }


}
?>
