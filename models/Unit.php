<?php // Stan 2013-09-26

namespace app\models;


class Unit extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'units';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getEntries()
  {
    return $this->hasMany('Entry', array('_units_id' => 'id'));
  }


}
?>
