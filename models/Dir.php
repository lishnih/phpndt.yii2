<?php // Stan 2013-09-17

namespace app\models;


class Dir extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'dirs';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getFiles()
  {
    return $this->hasMany('File', array('_dirs_id' => 'id'));
  }


}
?>
