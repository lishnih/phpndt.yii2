<?php // Stan 2013-09-17

namespace app\models;


class Handler extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'handlers';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getFileProcessings()
  {
    return $this->hasMany('FileProcessing', array('_handlers_id' => 'id'));
  }


}
?>
