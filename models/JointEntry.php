<?php // Stan 2013-05-24

namespace app\models;


class JointEntry extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'joint_entries';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getDoc()
  {
    return $this->hasOne('Doc', array('id' => '_docs_id'));
  }

  public function getJoint()
  {
    return $this->hasOne('Joint', array('id' => '_joints_id'));
  }


}
?>
