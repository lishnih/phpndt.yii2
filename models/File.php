<?php // Stan 2013-09-17

namespace app\models;


class File extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'files';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getDir()
  {
    return $this->hasOne('Dir', array('id' => '_dirs_id'));
  }

  public function getFileProcessings()
  {
    return $this->hasMany('FileProcessing', array('_files_id' => 'id'));
  }

  public function getSheets()
  {
    return $this->hasMany('Sheet', array('_files_id' => 'id'));
  }


}
?>
