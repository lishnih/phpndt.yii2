<?php // Stan 2013-05-24

namespace app\models;


class Doc extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'docs';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getSheet()
  {
    return $this->hasOne('Sheet', array('id' => '_sheets_id'));
  }

  public function getEntries()
  {
    return $this->hasMany('Entry', array('_docs_id' => 'id'));
  }

  public function getJointEntries()
  {
    return $this->hasMany('JointEntry', array('_docs_id' => 'id'));
  }


}
?>
