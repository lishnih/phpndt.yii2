<?php // Stan 2013-09-26

namespace app\models;


class Entry extends \yii\db\ActiveRecord
{


  public static function tableName()
  {
    return 'entries';
  }

  public static function primaryKey()
  {
    return array('id');
  }


  public function getDoc()
  {
    return $this->hasOne('Doc', array('id' => '_docs_id'));
  }

  public function getUnit()
  {
    return $this->hasOne('Unit', array('id' => '_units_id'));
  }


}
?>
