<?php // Stan 2013-05-24
use \yii\web\Controller;
use \yii\base\HttpException;

// use app\models\Doc;
// use app\models\Joint;

require_once 'lib/meta_array.php';
require_once 'lib/sql.php';
require_once 'lib/view.php';
require_once 'lib/common.php';


class SiteController extends Controller {
  /**
  Просмотр значений колонок всех таблиц
   */
  public function actionIndex() {
    $connection = \Yii::$app->db;

    list($route, $params) = \Yii::$app->request->resolve();
    unset($params['r']);

    $dbparams = get_dbparams($connection, $params);

    echo $this->render('index', array(
      'params' => $params,
      'dbparams' => $dbparams,
    ));
  }

}

?>
