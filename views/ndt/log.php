<?php // Stan 2013-10-13

use yii\helpers\Html;

$this->title = 'Log';
$this->params['breadcrumbs'][] = 'NDT';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  echo Html::a('Return', array_merge(array('view/index'), $params), array('class' => 'btn'));

?>
</div>
<br /><hr />


<?php

  if ( $rows ) {
    start_table( '№ п/п',
'№ стыка по журналу или сварочной схеме, № журнала или схемы',
'Шифр сварщика или бригады, фамилия, инициалы',
'',
'Дата и способ контроля (Р, Г, М, У)',
'№ и дата выдачи',
'Заключение о годности (годен, исправить, вырезать, повторный контроль)',
'радиографа',
'начальника или инженера службы контроля качества',
'Замечания контролирующих лиц по качеству выполняемого контроля физическими методами сварных соединений с указанием фамилии, должности и даты записи',
'Дата и подпись ответственного лица об устранении нарушений' );

    $table = 'joints';
  
    $seq = 1;
    foreach ( $rows as $row ):
  
//    echo_row( $row, $seq, $table, $params );

      $joints_name = $row['joints_name'];

      $welders   = array();
      $welders_r = array();

      $welder_text = '';
      $welder_r_text = '';

      if ( $row['welders_root'] )
        foreach ( explode('/', $row['welders_root'] ) as $welder_id )
          $welders[] = $welder_id;
      else {
        $welders[] = '';
        $welders[] = '';
      }

      if ( $row['welders_fill1'] )
        foreach ( explode('/', $row['welders_fill1'] ) as $welder_id )
          $welders[] = $welder_id;
      else {
        $welders[] = '';
        $welders[] = '';
      }

      if ( $row['welders_fill2'] )
        foreach ( explode('/', $row['welders_fill2'] ) as $welder_id )
          $welders[] = $welder_id;
      else {
        $welders[] = '';
        $welders[] = '';
      }

      if ( $row['welders_cap'] )
        foreach ( explode('/', $row['welders_cap'] ) as $welder_id )
          $welders[] = $welder_id;
      else {
        $welders[] = '';
        $welders[] = '';
      }

      if ( $row['welder_r'] )
        foreach ( explode('/', $row['welder_r']) as $welder_id )
          $welders_r[] = $welder_id;

      foreach ( $welders as $welder_id ) {
        if ( $welder_id ) {
          $werder = get_weldername( $welder_id );
          $welder_text .= $welder_id . ' - ' . $werder . '<br />';
        } else
          $welder_text .= '<br />';
      } // foreach

      foreach ( $welders_r as $welder_id ) {
        $werder = get_weldername( $welder_id );
        $welder_r_text .= $welder_id . ' - ' . $werder . '<br />';
      } // foreach

      $date_type = $row['date_str'] . '<br>' . $row['type'];
      $docname_date = $row['docs_name'] . '<br>' . $row['date_str'];

//    $date_type = '<table><tr><td>' . $row['date_str'] . '<tr><td>' . $row['type'] . '</table>';
//    $docname_date = '<table><tr><td>' . $row['docs_name'] . '<tr><td>' . $row['date_str'] . '</table>';

      $row = array( $joints_name,
                    $welder_text,
                    $welder_r_text,
                    $date_type,
                    $docname_date,
                    $row['decision'],
                    '', '', '', '' );

      echo_row( $row, $seq, $table, $params );
      $seq++;

    endforeach;
  
    stop_table();
  } else
    echo '<i>Нет данных!</i><br />';


function get_weldername( $welder_id ) {
  switch ( $welder_id ) {
    case '': $werder = ''; break;
    case '004': $werder = 'Антонов Э.В.'; break;
    case '005': $werder = 'Панченко А.В.'; break;
    case '006': $werder = 'Канаев А.Б.'; break;
    case '007': $werder = 'Менделев Б.А.'; break;
    case '008': $werder = 'Васько А.Н.'; break;
    case '009': $werder = 'Волков Ю.П.'; break;
    case '010': $werder = 'Гимадеев Л.Р.'; break;
    case '011': $werder = 'Жиглов А.Ю.'; break;
    case '012': $werder = 'Серов В.Е.'; break;
    case '013': $werder = 'Болотский А.В.'; break;
    case '014': $werder = 'Басыров А.Р.'; break;
    case '015': $werder = 'Симоненко Ю.В.'; break;
    case '016': $werder = 'Демченко Н.М.'; break;
    case '017': $werder = 'Скачков Д.В.'; break;
    case '018': $werder = 'Макаренко А.С.'; break;
    case '019': $werder = 'Кукаркин В.И.'; break;
    case '020': $werder = 'Гуща Д.В.'; break;
    case '021': $werder = 'Вуколов В.Н.'; break;
    case '022': $werder = 'Хабутдинов М.Н.'; break;
    case '023': $werder = 'Кущенко А.М.'; break;
    case '024': $werder = 'Марков А.А.'; break;
    case '025': $werder = 'Марков М.А.'; break;
    case '026': $werder = 'Стенников С.А.'; break;
    case '027': $werder = 'Жуков А.А.'; break;
    case '028': $werder = 'Кудряшов Н.И.'; break;
    case '029': $werder = 'Ярулин Т.Р.'; break;
    case '030': $werder = 'Алексеев А.В.'; break;
    case '031': $werder = 'Шукшин А.А.'; break;
    case '032': $werder = 'Кабанец А.В.'; break;
    case '033': $werder = 'Стародубов С.В.'; break;
    case '034': $werder = 'Копченко В.А.'; break;
    case '035': $werder = 'Арманаш А.П.'; break;
    case '036': $werder = 'Крылов С.Н.'; break;

    default: $werder = '-';
  } // switch
  
  return $werder;
} // function

?>
