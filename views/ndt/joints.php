<?php // Stan 2013-08-27

use yii\helpers\Html;

$this->title = 'Joints';
$this->params['breadcrumbs'][] = 'NDT';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  echo Html::a('Index',  array_merge(array('view/index'), $params), array('class' => 'btn'));
  echo Html::a('Docs',   array_merge(array('ndt/docs'),   $params), array('class' => 'btn'));
  echo Html::a('Joints', array_merge(array('ndt/joints'), $params), array('class' => 'btn'));
  echo Html::a('Units',  array_merge(array('ndt/units'),  $params), array('class' => 'btn'));

  echo Html::a('Reset', array($params['r']), array('class' => 'btn'));

?>
</div>
<br /><hr />


<?php

  if ( isset( $rows ) ) {
    start_table();
  
    $columns = array('id', 'name');

    ////////
    // TH //
    ////////
    echo "  <tr>\n";
    echo "    <th>#</th>\n";

    foreach ( $columns as $column ) {
      echo '    <th>' . $column . "</th>\n";
    } // foreach

    foreach ( $methods as $method ) {
      echo '    <th>' . $method['type'] . "</th>\n";
    } // foreach

    echo "  </tr>\n";

    ////////
    // TD //
    ////////
    $seq = 1;
    foreach ( $rows as $row ):
      $docs = array_pop( $row );

      echo "  <tr>\n";
      echo '    <td class=null>' . $seq . "</td>\n";
      $seq++;

      $beginning = True;
      foreach ( $row as $column ) {
        if ( $beginning ) {
          $params = array('id' => $column);
          $column = Html::a('<i>' . $column . '</i>', array_merge(array('one/joints'), $params));
          $beginning = False;
        } // if
        echo '    <td>' . $column . "</td>\n";
      } // foreach

      foreach ( $methods as $method ):
        $type = $method['type'];
        if ( array_key_exists( $type, $docs ) ) {
          echo "    <td>\n";
          start_table();

          foreach ( $docs[$type] as $doc ) {
            $name = $doc[1] . ' ' . $doc[0] . '<br /><i><b>' . $doc[2] . '</b></i>';
            if ( $doc[3] )
              $name .= '<br /><i>' . $doc[3] . '</i>';
            echo '    <tr><td>' . $name . "</td></tr>\n";
//          echo '    <tr><td>' . sprint_ra($doc) . "</td></tr>\n";
          } // foreach

          stop_table();
          echo "    </td>\n";
        } else
          echo "    <td>-</td>\n";
      endforeach;

      echo "  </tr>\n";
  
    endforeach;
  
    stop_table();
  } // if

?>
