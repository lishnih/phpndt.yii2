<?php // Stan 2013-08-27

use yii\helpers\Html;

$this->title = 'Docs';
$this->params['breadcrumbs'][] = 'NDT';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  echo Html::a('Index',  array_merge(array('view/index'), $params), array('class' => 'btn'));
  echo Html::a('Docs',   array_merge(array('ndt/docs'),   $params), array('class' => 'btn'));
  echo Html::a('Joints', array_merge(array('ndt/joints'), $params), array('class' => 'btn'));
  echo Html::a('Units',  array_merge(array('ndt/units'),  $params), array('class' => 'btn'));

  echo Html::a('Reset', array($params['r']), array('class' => 'btn'));

?>
</div>
<br /><hr />


<?php

  table_rows( $rows, 'docs', $params );

?>
