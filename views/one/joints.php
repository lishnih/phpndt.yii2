<?php // Stan 2013-09-26

use yii\helpers\Html;

$this->title = 'Joint';
$this->params['breadcrumbs'][] = 'One';
$this->params['breadcrumbs'][] = $this->title;


echo '<i>Joint:</i>' . "<br />\n";
table_rows( [$joint], 'joints', $params );
 
echo '<i>Joint Entries:</i>' . "<br />\n";
table_rows( $joint_entries, 'joint_entries', $params );

?>
