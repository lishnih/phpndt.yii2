<?php // Stan 2013-09-17

use yii\helpers\Html;

$this->title = 'Doc';
$this->params['breadcrumbs'][] = 'One';
$this->params['breadcrumbs'][] = $this->title;


echo '<i>Dir:</i>' . "<br />\n";
table_rows( [$dir], 'dirs', $params );
 
echo '<i>File:</i>' . "<br />\n";
table_rows( [$file], 'files', $params );
 
echo '<i>Handler:</i>' . "<br />\n";
table_rows( [$handler], 'handlers', $params );

echo '<i>File Processing:</i>' . "<br />\n";
table_rows( [$fileprocessing], 'fileprocessings', $params );

echo '<i>Sheet:</i>' . "<br />\n";
table_rows( [$sheet], 'sheets', $params );
 
echo '<i>Doc:</i>' . "<br />\n";
table_rows( [$doc], 'docs', $params );
 
echo '<i>Entries:</i>' . "<br />\n";
table_rows( $entries, 'entries', $params );

echo '<i>Joint Entries:</i>' . "<br />\n";
table_rows( $joint_entries, 'joint_entries', $params );

?>
