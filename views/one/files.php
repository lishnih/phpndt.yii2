<?php // Stan 2013-09-17

use yii\helpers\Html;

$this->title = 'File';
$this->params['breadcrumbs'][] = 'One';
$this->params['breadcrumbs'][] = $this->title;


echo '<i>Dir:</i>' . "<br />\n";
table_rows( [$dir], 'dirs', $params );
 
echo '<i>File:</i>' . "<br />\n";
table_rows( [$file], 'files', $params );
 
echo '<i>File Processings:</i>' . "<br />\n";
table_rows( $fileprocessings, 'fileprocessings', $params );

// echo '<i>Sheets:</i>' . "<br />\n";
// table_rows( $sheets, 'sheets', $params );

?>
