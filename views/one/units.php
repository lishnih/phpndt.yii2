<?php // Stan 2013-09-26

use yii\helpers\Html;

$this->title = 'Unit';
$this->params['breadcrumbs'][] = 'One';
$this->params['breadcrumbs'][] = $this->title;


echo '<i>Unit:</i>' . "<br />\n";
table_rows( [$unit], 'units', $params );
 
echo '<i>Entries:</i>' . "<br />\n";
table_rows( $entries, 'entries', $params );

?>
