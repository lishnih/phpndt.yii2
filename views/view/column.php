<?php // Stan 2013-09-24

use yii\helpers\Html;

$this->title = 'Column';
$this->params['breadcrumbs'][] = 'View';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  unset($params['table']);
  unset($params['column']);

  echo Html::a('Return', array_merge(array('view/index'), $params), array('class' => 'btn'));

?>
</div>
<br /><hr />


<?php

  echo "Records: " . $all . "<br /><br />\n";

  $params_d = $params;
  unset($params_d[$table][$column]);

  start_table();

  foreach ( $rows as $row ):

    if ( $row === NULL ) {
      $value = '<i>NULL</i>';
      $params_d[$table][$column] = array('is' => 'NULL');
    } elseif ( $row === '' ) {
      $value = '<i>empty</i>';
      $params_d[$table][$column] = $row;
    } else {
      $value = $row;
      $params_d[$table][$column] = $row;
    } // if

    echo "  <tr>\n";
    echo '<td>' . Html::a( $value, array_merge(array('view/index'), $params_d) ) . '</td>';
    echo '<td>' . gettype( $row ) . '</td>';
    $hexes = str_split( bin2hex( $row ), 2 );
    $text = "";   // $text = join( ' ', $hexes );
    foreach ( $hexes as $hex ) {
      $ord = hexdec( $hex );
      $title = htmlspecialchars( chr( $ord ) );
      $text .= '<span title="' . $title . '">' . $hex. '</span> ';
    } // foreach
    echo '<td>' . $text . "</td>\n";
    echo "  </tr>\n";

  endforeach;

  stop_table();

?>
