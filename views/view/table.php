<?php // Stan 2013-05-26

use yii\helpers\Html;

$this->title = 'Table';
$this->params['breadcrumbs'][] = 'View';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  echo Html::a('Index', array_merge(array('view/index'), $params), array('class' => 'btn'));

//echo Html::a('Reset', array($params['r']), array('class' => 'btn'));
  $r = array($params['r']);
  $params = array('table' => $params['table']);
  echo Html::a('Reset', array_merge($r, $params), array('class' => 'btn'));

?>
</div>
<br /><hr />


<?php

  echo 'Records: ' . $all . "<br />\n";
  echo 'Shown: ' . $count . "<br /><br />\n";

  table_rows( $rows, $params['table'], $params );

?>
