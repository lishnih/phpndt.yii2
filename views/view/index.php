<?php // Stan 2013-05-24

use yii\helpers\Html;

$this->title = 'View';
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  echo Html::a('Index',  array_merge(array('view/index'), $params), array('class' => 'btn'));
  echo Html::a('Docs',   array_merge(array('ndt/docs'),   $params), array('class' => 'btn'));
  echo Html::a('Joints', array_merge(array('ndt/joints'), $params), array('class' => 'btn'));
  echo Html::a('Units',  array_merge(array('ndt/units'),  $params), array('class' => 'btn'));

  echo Html::a('Reset', array($params['r']), array('class' => 'btn'));

?>
</div>

<div class="pull-right btn-group">
<?php

  echo Html::a('Debug', array_merge(array('view/debug'), $params), array('class' => 'btn'));

?>
</div>

<br /><hr />


<?php if(Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-error">
  <?php echo Yii::$app->session->getFlash('error'); ?>
</div>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
  <?php echo Yii::$app->session->getFlash('success'); ?>
</div>
<?php endif; ?>


<?php

  foreach ( $tables as $tablename => $tableinfo ):
    $tableparams = $metaArray->get($tablename);

    list($count, $all, $columns) = $tableinfo;

    $a = Html::a($tablename, array_merge(array('view/table'), $params, array('table' => $tablename)));
    echo <<<EOD
<fieldset><legend>$a ($count)</legend>
<table border="1">\n
EOD;

    foreach ( $columns as $columnname => $columninfo ):
      list($columncount, $columntype, $values) = $columninfo;

      if ( isset( $tableparams[$columnname] ) ) {
        $params_d = $params;
        unset($params_d[$tablename][$columnname]);
        $reset_a = Html::a('<i>Сбросить</i>', array_merge(array(''), $params_d));
      } else
        $reset_a = '';

      if ( $reset_a )
        echo '<tr><td><b><i>' . $columnname . '</i></b><td>' . $columncount . "<td>\n";
      else
        echo '<tr><td>' . $columnname . '<td>' . $columncount . "<td>\n";

      $valuesshow = 0;
      $columnlen = 0;
      foreach ( $values as $value ):
        $params_d = $params;

        if ( $value === NULL ) {
          $params_d[$tablename][$columnname] = array('is' => 'NULL');
          $value = '<i>NULL</i>';
        } elseif ( $value === '' ) {
          $params_d[$tablename][$columnname] = $value;
          $value = '<i>empty</i>';
        } else
          $params_d[$tablename][$columnname] = $value;
        echo Html::a('[' . $value . ']', array_merge(array(''), $params_d));
        if ( strlen($value) > 80 )
          echo "<br />\n";
        else
          echo ' ';

        $valuesshow += 1;

        $columnlen += strlen($value);
        if ( $columnlen > 1000 )
          break;
      endforeach;

      if ( $valuesshow < $columncount )
        echo '...(' . $valuesshow . ') ' .
             Html::a('<i>Показать все</i>', array_merge(array('view/column'),
                     $params, array('table' => $tablename, 'column' => $columnname))) .
             "\n";

      echo $reset_a . "</td></tr>\n";

    endforeach;

    echo "</table></fieldset><br />\n";

  endforeach;

?>
