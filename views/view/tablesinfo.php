<?php // Stan 2013-05-26

use yii\helpers\Html;

$this->title = 'Tables Info';
$this->params['breadcrumbs'][] = 'View';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  echo Html::a('Index', array_merge(array('view/index'), $params), array('class' => 'btn'));
  echo Html::a('DB Info', array('view/db-info'), array('class' => 'btn'));

?>
</div>
<br /><hr />


<?php

  echo Html::a('Сведения о базе данных', array('view/db-info'), array('class' => 'btn'));
  echo "<br /><br />\n";

  // Выводим $schemas в виде таблицы без поля 'columns'
  start_table();

  $headers = array();
  $columns = array();
  foreach ( $schemas as $schema ):
    $columns[$schema->name] = $schema->columns;

    if ( !$headers ) {
      echo "  <tr>\n";
      foreach ( $schema as $key => $value ):
        if ( $key == 'columns' )
          continue;

        echo '    <th>' . $key . "</th>\n";
        $headers[] = $key;
      endforeach;
      echo "  </tr>\n";
    } // if

    echo "  <tr>\n";
    foreach ( $headers as $key ) {
      echo '    <td>' . sprint_ra( $schema->$key ) . "</td>\n";
    } // foreach
    echo "  </tr>\n";

  endforeach;

  stop_table();


  echo "<br />\n";


  // Выводим поле 'columns' в виде таблицы
  start_table();

  $headers = array();
  foreach ( $columns as $tablename => $column ):
    $count = count($column);
    foreach ( $column as $columnname => $columninfo ):
  
      if ( !$headers ) {
        echo "  <tr>\n";
        echo "    <th>#</th>\n";
//         echo "    <th>#</th>\n";
        foreach ( $columninfo as $key => $value ) {
          echo '    <th>' . $key . "</th>\n";
          $headers[] = $key;
        } // foreach
        echo "  </tr>\n";
      } // if
  
      echo "  <tr>\n";
      if ( $count ) {
        echo '    <td rowspan="' . $count . '">' . $tablename . "</td>\n";
        $count = 0;
      } // if
//       echo '    <td>' . $columnname . "</td>\n";
      foreach ( $headers as $key ) {
        echo '    <td>' . sprint_ra( $columninfo->$key ) . "</td>\n";
      } // foreach
      echo "  </tr>\n";
  
    endforeach;
  endforeach;

  stop_table();

?>
