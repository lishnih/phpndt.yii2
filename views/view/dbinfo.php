<?php // Stan 2013-05-26

use yii\helpers\Html;

$this->title = 'DB Info';
$this->params['breadcrumbs'][] = 'View';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pull-left btn-group">
<?php

  echo Html::a('Index', array_merge(array('view/index'), $params), array('class' => 'btn'));
  echo Html::a('Tables Info', array('view/tables-info'), array('class' => 'btn'));

?>
</div>
<br /><hr />


<?php

  // Выводим $connection
  print_ra($connection);

?>
