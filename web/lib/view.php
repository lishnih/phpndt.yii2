<?php // Stan 2007-04-06

use yii\helpers\Html;


// Выводит начало таблицы
function start_table ( ) {
global $started_tables;
  $table_options = array();
  $table_options['dont_close_thead'] = False;

  $table_id = count( $started_tables ) + 1;
  $this_table =& $started_tables[$table_id];

  echo '<table cellpadding="0" cellspacing="1" border="1" id="table' . $table_id . '" class="tablesorter">';

  $ncols = func_num_args();
  if ( $ncols )
    echo "\n <thead>\n  <tr>\n";

  for ( $i = 0; $i < $ncols; $i++ ) {
    $arg = func_get_arg( $i );
    if ( is_array( $arg ) ) {
      foreach ( $arg as $key => $row )
        if ( $key )
          switch ( $key ) {
            case 'id':
              $this_table['id'] = $row;
              $this_table[$i][$key] = $row;
              break;
            case '!dont_close_thead':
              $table_options['dont_close_thead'] = $row;
              break;
            default:
              $this_table[$i][$key] = $row;
          } // switch
        else
          $str = $row;
    } else
      $str = $arg;
    echo "   <th>$str</th>\n";
  } // for

  if ( !$table_options['dont_close_thead'] )
    echo "  </tr>\n </thead>\n <tbody>\n";

  return $ncols;
} // function


// Выводит ряд таблицы
function echo_row ( $row, $seq = NULL, $table = NULL, $params = NULL ) {
  echo '  <tr>';
  if ( $seq !== NULL )
    echo '    <td class=null>' . $seq . "</td>\n";

  foreach ( $row as $key => $column ) {
    if ( $key == 'id' ) {
      if ( $table and $params ) {
        $params['id'] = $column;
        $column = Html::a('<i>' . $column . '</i>', array_merge(array('one/'.$table), $params));
      } else
        $column = '<i>' . $column . '</i>';
    } elseif ( $key[0] == '_' ) {
      if ( $table and $params ) {
        $table = substr( $key, 1, strpos( $key, '_', 1 ) - 1 );
        $params['id'] = $column;
        $column = Html::a('<i>' . $column . '</i>', array_merge(array('one/'.$table), $params));
      } else
        $column = '<i>' . $column . '</i>';
    } // if

    if ( $column === NULL )
      echo "    <td class=null>NULL</td>\n";
    else
      echo '    <td>' . $column . "</td>\n";
  } // foreach
  echo "  </tr>\n";
} // function


// Выводит конец таблицы
function stop_table ( ) {
global $started_tables;
  echo " </tbody>\n</table>\n";
  $table_id = count( $started_tables );
//   print_ra( $started_tables );
  unset( $started_tables[$table_id] );
} // function


function table_rows ( $rows, $table = NULL, $params = NULL ) {
  if ( $rows ) {
    start_table();
  
    $seq = 1;
    foreach ( $rows as $row ):
  
      if ( $seq == 1 ) {
        echo "  <tr>\n";
        echo "    <th>#</th>\n";
        foreach ( $row as $key => $column ) {
          echo '    <th>' . $key . "</th>\n";
        } // foreach
        echo "  </tr>\n";
      } // if
  
      echo_row( $row, $seq, $table, $params );
      $seq++;

    endforeach;
  
    stop_table();
  } else
    echo '<i>Нет данных!</i><br />';

  echo "<br />\n";
} // function

?>
