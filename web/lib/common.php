<?php // Stan 2004-08-09

/**
* @package      PHP_Common
* @version      0.26
* @date         2011-02-16
*
* @author       Stan Ovchinnikov
* @email        lishnih@gmail.com
* @copyright    2004—2011
*/

// Скрипт объявляет глобальные переменные:
// $phpcommon_errors
// $phpcommon_old_error_handler

// Функции:
// phpCommonExitHandler phpCommonErrorHandler
// print_rb print_ra  print_rt print_sh
//          sprint_ra sprint_rt

// И следующие константы:
   define( 'PHP_COMMON', dirname( __FILE__ ) );     // Вход с скрипт
   define( 'ERROR',      E_USER_ERROR        );     // Ошибка
   define( 'WARNING',    E_USER_WARNING      );     // Предупреждение
   define( 'NOTICE',     E_USER_NOTICE       );     // Диалог пользователя

// define( 'PHPCOMMON_ERRORSMAILTO',  'root@localhost' );
   define( 'PHPCOMMON_ERRORSMAILFLAG', E_ALL ^ E_USER_NOTICE );
// Если произойдёт любая из указанных ошибок (PHPCOMMON_ERRORSMAILFLAG),
// то будет отправлен отчёт получателю (PHPCOMMON_ERRORSMAILTO)
// Закомментируйте мыло или флаги, если не хотите получать отчёты

// Некоторые параметры для скрипта передаются через константы
// PHPCOMMON_ALWAYSEXPANDEDSTRING - Обработчик ошибок выведет развёрнутый текст

// ob_start( 'ob_gzhandler' );      // Добавляем сжатие страницы
   error_reporting ( E_ALL );       // Ошибки, которые будут выводиться на экран


/////////////////////////////////////////
//////// Функции вывода массивов ////////
/////////////////////////////////////////
// Эта функция подобна вызывает print_ra для всех переданных аргументов
function print_rb () {
  echo '<table border="1">';
  for ( $i = 0; $i < func_num_args(); $i++ ) {
    echo "\n<tr><td>$i<td>";
    print_ra( func_get_arg( $i ) );
  } // for
  echo "\n</table>\n";
} // function

// Обёртка к функции print_r
function print_ra ( $array, $params = null ) {
  echo sprint_ra( $array, $params );
} // function

function sprint_ra ( $array, $params = null ) {
  if ( $array === False ) return '<i>False</i>';
  if ( $array === True  ) return '<i>True</i>';
  if ( $array === NULL  ) return '<i>NULL</i>';
  if ( $array === ''    ) return '<i>empty</i>';

  if ( is_numeric( $params ) )
    $max_string = (int) $params;
  elseif ( is_string( $params ) )
    $params = explode( ',', $params );

  if ( is_object( $array ) or is_array( $array ) ) {
    $content = '<table border="1">';
    foreach ( $array as $key => $val ) {
      $content .= "\n <tr><td>$key<td>";
      $content .= sprint_ra( $val, $params );
    } // foreach
    $content .= "\n</table>\n";
  } elseif ( is_string( $array ) ) {
    if ( is_array( $params ) )
      foreach ( $params as $val ) {
        if     ( is_numeric( $val )  ) $max_string = (int) $val;
        elseif ( $val == 'htmlchars' ) $htmlchars = true;
        elseif ( $val == 'strip'     ) $strip = true;
        elseif ( $val == 'nopre'     ) $nopre = true;
      } // foreach
    if ( isset( $htmlchars ) )
      $array = htmlspecialchars( $array );
    if ( isset( $strip ) )
      $array = strip_tags( $array );
    if ( isset( $max_string ) and strlen( $array ) > $max_string ) {
      $array = strip_tags( $array );
      $array = substr( $array, 0, $max_string ) . '<span title="' . strip_tags( $array ) . '">...</span>';
    } // if
    $content = isset( $nopre ) ? $array : "<pre>$array</pre>";
  } else
    $content = $array;
  return $content;
} // function

// Эта функция выводит структурированные массивы в виде таблицы
// действительно только для массивов вида array( 0 => array(), 1 => array() )
function print_rt ( $array ) {
  echo sprint_rt( $array );
} // function

function sprint_rt ( $array ) {
  if ( !is_array( $array ) ) return '<i>must be array</i>';
  if ( $array === ''       ) return '<i>empty</i>';

  if ( !is_array( current( $array ) ) )
    $array = array( '' => $array );
  $content = '<table border="1">
<tr>';
  $a_keys = array_keys( current( $array ) );
  $content .= "\n <th>#";
  foreach ( $a_keys as $val )
    $content .= "\n <th>$val";
  foreach ( $array as $key => $val ) {
    $content .= "\n<tr>\n <td><i>$key</i>";
    foreach ( $val as $val )
      $content .= "\n <td>" . sprint_ra( $val, 'nopre' );
  } // foreach
  $content .= "\n</table>\n";
  return $content;
} // function

//////////////////////////////////////
//////// Функции вывода строк ////////
//////////////////////////////////////
// Эта функция выводит hex-коды символов строки
function print_sh ( $str, $c = 16 ) {
    echo '<table border="1">
<tr><td><table border="1" cellpadding="3">';
    for ( $i = 0; $i < strlen( $str ); $i++ ) {
      if ( $i % $c == 0 )
        echo "\n<tr>";
      echo '<td>' . sprintf( '%02X', ord( $str[$i] ) );
    } // for
    echo '</table>
<td><table border="1" cellpadding="3">';

  if ( function_exists( 'mb_detect_encoding' ) ) {
    $ec = mb_detect_encoding( $str );
    echo "\n<tr>";
    for ( $i = $j = 0; $i < mb_strlen( $str, $ec ); $i++, $j += $cl ) {
      if ( $j >= $c ) {
        echo "\n<tr>";
        $j -= $c;
        if     ( $j > 1 ) echo "<td colspan=$j>";
        elseif ( $j     ) echo "<td>";
      } // if
      $char = mb_substr( $str, $i, 1, $ec );
      $cl = strlen( $char );
      echo $cl > 1 ? "<td colspan=$cl>" : '<td>';
      echo ord( $char ) < 32 ? '[]' : $char;
    } // for
  } else {
    for ( $i = 0; $i < strlen( $str ); $i++ ) {
      if ( $i % $c == 0 )
        echo "\n<tr>";
      echo '<td>';
      echo ord( $str[$i] ) < 32 ? '[]' : $str[$i];
    } // for
  } // if

  echo '</table>
</table>';
  if ( isset( $ec ) )
    echo "\nКодировка: $ec<br />\n";
} // function

?>
