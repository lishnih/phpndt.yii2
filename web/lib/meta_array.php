<?php // Stan 2008-05-17

/**
* @package      phpNDT
* @name         MetaArray
* @version      2.0
* @date         2013-09-25
*
* @author       Stan
* @email        lishnih@gmail.com
* @copyright    2006—2013
* @license      MIT
*/


// Класс MetaArray
// Данный класс работает в связке с массивом $_GET и предназначен
// для хранения условий переданных через массив по внутреннему протоколу


class MetaArray {
  var $classname = 'MetaArray'; // Имя класса
  var $ver = '2.0';             // Версия
  var $meta_array = array();    // Массив хранения условий
  var $tables = array();        // Массив импортированных таблиц


  // Конструктор
  // $get_array - массив условий по умолча $_GET
  // $tables - какие ветки необходимо извлечь из $get_array
  // если $tables не задан, добавятся все ветки из $get_array
  function MetaArray ( $get_array = NULL, $tables = NULL ) {
    if ( $get_array === NULL )
      $get_array = $_GET;

    if ( $tables and !is_array( $tables ) )
      $tables = explode( ',', $tables );

    foreach ( $get_array as $table => $columns_expr )
      if ( $tables === NULL or in_array( $table, $tables ) )
        if ( is_array( $columns_expr ) )
          $this->meta_array[$table] = $columns_expr;

    $this->tables = array_keys($this->meta_array);
  } // function


  // Возращает True, если $meta_array пустой
  function is_empty ( ) {
    return $this->meta_array ? False : True;
  } // function


  // Возращает заданную ветку или ключ в ветке $meta_array
  function get ( $table = NULL, $column = NULL ) {
    if ( $column !== NULL )
      return isset( $this->meta_array[$table][$column] ) ?
             $this->meta_array[$table][$column] : NULL;

    if ( $table !== NULL )
      return isset( $this->meta_array[$table] ) ?
             $this->meta_array[$table] : array();

    return $this->meta_array;
  } // function


  // Возращает строку (с формате SQL) для заданных таблиц $meta_array
  function serialize_sql ( $tables = NULL, $table_prefix = '' ) {
    if ( $this->is_empty() )
      return '1';

    if ( $tables === NULL )
      $tables = $this->tables;

    if ( !is_array( $tables ) )
      $tables = explode( ',', $tables );

    $str = '';
    foreach ( $tables as $table ) {
      if ( in_array( $table, $this->tables ) ) {
        foreach ( $this->get( $table ) as $column => $columns_expr ) {
          $tablename = $table_prefix ? $table_prefix . '_' . $table : $table;

          $expr = $this->serialize_expr_sql( $columns_expr );
          if ( $expr ) {
            if ( $str )
              $str .= ' AND ';
            $str .= $tablename . '.' . $column . ' ' . $expr;
          } // if
        } // foreach
      } // if
    } // foreach

    if ( !$str )
      $str = '1';

    return $str;
  } // function


  // Возращает строку (с формате GET) для заданных таблиц $meta_array
  function serialize_get ( $tables = NULL ) {
    if ( $tables === NULL )
      $tables = $this->tables;

    if ( !is_array( $tables ) )
      $tables = explode( ',', $tables );

    $str = '';
    foreach ( $tables as $table ) {
      $str1 = MetaArray::serialize_array_get( $this->get( $table ), $table );

      if ( $str1 ) {
        if ( $str )
          $str .= '&';
        $str .= $str1;
      } // if
    } // foreach

    return $str;
  } // function


  // Возращает строку (с формате SQL) из $array (в формате $meta_array)
  function serialize_expr_sql ( $column_expr ) {
    if ( is_array( $column_expr ) ) {
      $operand = key( $column_expr );
      $params = $column_expr[$operand];
  
      switch ( $operand ) {
        case '<<':
          $str = '<= \'' . $params . '\'';
          break;
        case '>>':
          $str = '>= \'' . $params . '\'';
          break;
        case '!':
          $str = '!= \'' . $params . '\'';
          break;
        case '~':
        case '%':
          $str = 'LIKE \'' . $params . '\'';
          break;
        case '%%':
          $str = 'LIKE \'%' . $params . '%\'';
          break;
        case 'is':
          $str = 'IS ' . $params;
          break;
        case '!is':
          $str = 'NOT IS ' . $params;
          break;
        default:
          $str = $operand . ' \'' . $params . '\'';
      } // switch
  
      return $str;
    } else
      return '= \'' . $column_expr . '\'';
  } // function


    // Возращает строку (с формате GET) из $array (в формате $meta_array)
    function serialize_array_get ( $array, $prefix = '' ) {
      if ( is_array( $array ) ) {
        $str = '';
        foreach ( $array as $key => $val ) {
          if ( $prefix )
            $val = MetaArray::serialize_array_get( $val, $prefix . '[' . $key . ']' );   // !!!
          else
            $val = MetaArray::serialize_array_get( $val, $key );
          if ( $val ) {
            if ( $str )
              $str .= '&';
            $str .= $val;
          } // if
        } // foreach
        return $str;
      } else
        return $prefix . '=' . htmlspecialchars( $array );
    } // function
} // class

?>
