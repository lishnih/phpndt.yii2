<?php // Stan 2013-05-26


function get_schema( $connection, $tablename ) {
  $schemas = $connection->schema->tableSchemas;

  foreach ( $schemas as $schema ) {
    if ( $schema->name == $tablename )
      return $schema;
  } // foreach
} // function


function sql_count( $connection, $tablename, $select = '*', $metaArray = NULL ) {
  $where = $metaArray ? $metaArray->serialize_sql( $tablename ) : '1';

  $is_function = strpos($select, '(') !== FALSE || $select === '*';

  if ( $is_function )
    $sql = sprintf("SELECT COUNT(%s) as c FROM `%s` WHERE %s", $select, $tablename, $where);
  else
    $sql = sprintf("SELECT COUNT(`%s`) as c FROM `%s` WHERE %s", $select, $tablename, $where);

  $command = $connection->createCommand($sql);
  $count = $command->queryOne();
  $count = $count['c'];

  return $count;
} // function


function sql_count2( $connection, $tablename, $select = '*', $metaArray = NULL ) {
  $where = $metaArray ? $metaArray->serialize_sql( $tablename ) : '1';

  $is_function = strpos($select, '(') !== FALSE || $select === '*';

  if ( $is_function )
    $sql = sprintf("SELECT COUNT(%s) as c FROM `%s` WHERE %s", $select, $tablename, $where);
  else
    $sql = sprintf("SELECT COUNT(`%s`) as c FROM `%s` WHERE %s", $select, $tablename, $where);

  $command = $connection->createCommand($sql);
  $count = $command->queryOne();
  $count = $count['c'];

  if ( $is_function )
    $sql = sprintf("SELECT COUNT(%s) as c FROM `%s`", $select, $tablename);
  else
    $sql = sprintf("SELECT COUNT(`%s`) as c FROM `%s`", $select, $tablename);

  $command = $connection->createCommand($sql);
  $all = $command->queryOne();
  $all = $all['c'];

  return array($count, $all);
} // function


// Перечень значений для заданного поля $field в таблице $tablename
// Возможно задания условия $operand вместо равенства
function get_distinct ( $connection, $tablename, $columnname, $metaArray = NULL, $limit = 100 ) {
  $where = $metaArray ? $metaArray->serialize_sql( $tablename ) : '1';

  if ( $limit === null )
    $sql = sprintf("SELECT DISTINCT(`%s`) as d FROM `%s` WHERE %s ORDER BY `%s`",
             $columnname, $tablename, $where, $columnname);
  else    
    $sql = sprintf("SELECT DISTINCT(`%s`) as d FROM `%s` WHERE %s ORDER BY `%s` LIMIT 0, %s",
             $columnname, $tablename, $where, $columnname, $limit);

  $command = $connection->createCommand($sql);
  $reader = $command->query();

  $values = array();
  foreach ( $reader as $row ) {
    $values[] = $row['d'];
  } // foreach

  return $values;
} // function


function sql_select( $connection, $tablename, $select = '*', $metaArray = NULL, $sortby = NULL ) {
  $where = $metaArray ? $metaArray->serialize_sql( $tablename ) : '1';

  $is_function = strpos($select, '(') !== FALSE || $select === '*';

  if ( $sortby )
    $where_sort = sprintf("WHERE %s ORDER BY `%s`", $where, $sortby);
  else
    $where_sort = sprintf("WHERE %s", $where);

  if ( $is_function )
    $sql = sprintf("SELECT %s FROM `%s` %s", $select, $tablename, $where_sort);
  else
    $sql = sprintf("SELECT `%s` FROM `%s` %s", $select, $tablename, $where_sort);

  $command = $connection->createCommand($sql);
  $rows = $command->queryAll();

  return $rows;
} // function

?>
