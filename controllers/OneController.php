<?php // Stan 2013-09-17

namespace app\controllers;

use Yii;
use \yii\web\Controller;
use \yii\base\HttpException;

use app\models\Dir;
use app\models\File;
use app\models\Handler;
use app\models\FileProcessing;
use app\models\Sheet;
use app\models\Doc;
use app\models\Unit;
use app\models\Entry;
use app\models\Joint;
use app\models\JointEntry;

require_once 'lib/meta_array.php';
require_once 'lib/sql.php';
require_once 'lib/view.php';
require_once 'lib/common.php';


class OneController extends Controller {
  public $layout = '@app/views/layouts/view';


  /**
  Index
   */
  public function actionIndex() {
    list($route, $params) = Yii::$app->request->resolve();

    echo $this->render('debug', array(
      'params' => $params,
    ));
  }


  /**
  Dirs
   */
  public function actionDirs() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $dirs_tn = Dir::tableName();

    $id = $params['id'];
    $dir = Dir::find()
             ->where(array('id' => $id))
             ->one();

    $files = $dir
               ->getFiles()
               ->all();

    echo $this->render($dirs_tn, array(
      'params' => $params,
      'dir'    => $dir,
      'files'  => $files,
    ));
  }


  /**
  Files
   */
  public function actionFiles() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $files_tn = File::tableName();

    $id = $params['id'];
    $file = File::find()
              ->where(array('id' => $id))
              ->one();
 
    $dir = $file
             ->getDir()
             ->one();

    $fileprocessings = $file
                         ->getFileProcessings()
                         ->all();

//  $sheets = $file
//              ->getSheets()
//              ->all();

    echo $this->render($files_tn, array(
      'params'          => $params,
      'dir'             => $dir,
      'file'            => $file,
      'fileprocessings' => $fileprocessings,
//    'sheets'          => $sheets,
    ));
  }


  /**
  Handlers
   */
  public function actionHandlers() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $handlers_tn = Handler::tableName();

    $id = $params['id'];
    $handler = Handler::find()
                 ->where(array('id' => $id))
                 ->one();

    $fileprocessings = $handler
                         ->getFileProcessings()
                         ->all();

    echo $this->render($handlers_tn, array(
      'params'          => $params,
      'handler'         => $handler,
      'fileprocessings' => $fileprocessings,
    ));
  }


  /**
  File Processings
   */
  public function actionFileprocessings() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $fileprocessings_tn = FileProcessing::tableName();

    $id = $params['id'];
    $fileprocessing = FileProcessing::find()
                        ->where(array('id' => $id))
                        ->one();
 
    $file = $fileprocessing
            ->getFile()
            ->one();

    $dir = $file
             ->getDir()
             ->one();

    $handler = $fileprocessing
                 ->getHandler()
                 ->one();

    $sheets = $fileprocessing
                ->getSheets()
                ->all();

    echo $this->render($fileprocessings_tn, array(
      'params'         => $params,
      'dir'            => $dir,
      'file'           => $file,
      'handler'        => $handler,
      'fileprocessing' => $fileprocessing,
      'sheets'         => $sheets,
    ));
  }


  /**
  Sheets
   */
  public function actionSheets() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $sheets_tn = Sheet::tableName();

    $id = $params['id'];
    $sheet = Sheet::find()
               ->where(array('id' => $id))
               ->one();
 
    $fileprocessing = $sheet
                        ->getFileProcessing()
                        ->one();

    $file = $fileprocessing
              ->getFile()
              ->one();

    $dir = $file
             ->getDir()
             ->one();

    $handler = $fileprocessing
                 ->getHandler()
                 ->one();

    $docs = $sheet
              ->getDocs()
              ->all();

    echo $this->render($sheets_tn, array(
      'params'         => $params,
      'dir'            => $dir,
      'file'           => $file,
      'handler'        => $handler,
      'fileprocessing' => $fileprocessing,
      'sheet'          => $sheet,
      'docs'           => $docs,
    ));
  }


  /**
  Docs
   */
  public function actionDocs() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $docs_tn = Doc::tableName();

    $id = $params['id'];
    $doc = Doc::find()
             ->where(array('id' => $id))
             ->one();
 
    $sheet = $doc
               ->getSheet()
               ->one();

    $fileprocessing = $sheet
                        ->getFileProcessing()
                        ->one();

    $file = $fileprocessing
              ->getFile()
              ->one();

    $dir = $file
             ->getDir()
             ->one();

    $handler = $fileprocessing
                 ->getHandler()
                 ->one();

    $entries = $doc
                 ->getEntries()
                 ->all();

    $joint_entries = $doc
                       ->getJointEntries()
                       ->all();

    echo $this->render($docs_tn, array(
      'params'         => $params,
      'dir'            => $dir,
      'file'           => $file,
      'handler'        => $handler,
      'fileprocessing' => $fileprocessing,
      'sheet'          => $sheet,
      'doc'            => $doc,
      'entries'        => $entries,
      'joint_entries'  => $joint_entries,
    ));
  }


  /**
  Units
   */
  public function actionUnits() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $units_tn = Unit::tableName();

    $id = $params['id'];
    $unit = Unit::find()
              ->where(array('id' => $id))
              ->one();

    $entries = $unit
                 ->getEntries()
                 ->all();

    echo $this->render($units_tn, array(
      'params'  => $params,
      'unit'    => $unit,
      'entries' => $entries,
    ));
  }


  /**
  Entries
   */
  public function actionEntries() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $entries_tn = Entry::tableName();

    $id = $params['id'];
    $entry = Entry::find()
               ->where(array('id' => $id))
               ->one();
 
    $unit = $entry
             ->getUnit()
             ->one();

    $doc = $entry
             ->getDoc()
             ->one();

    $sheet = $doc
               ->getSheet()
               ->one();

    $fileprocessing = $sheet
                        ->getFileProcessing()
                        ->one();

    $file = $fileprocessing
              ->getFile()
              ->one();

    $dir = $file
             ->getDir()
             ->one();

    $handler = $fileprocessing
                 ->getHandler()
                 ->one();

    echo $this->render($entries_tn, array(
      'params'         => $params,
      'dir'            => $dir,
      'file'           => $file,
      'handler'        => $handler,
      'fileprocessing' => $fileprocessing,
      'sheet'          => $sheet,
      'doc'            => $doc,
      'unit'           => $unit,
      'entry'          => $entry,
    ));
  }


  /**
  Joints
   */
  public function actionJoints() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $joints_tn = Joint::tableName();

    $id = $params['id'];
    $joint = Joint::find()
               ->where(array('id' => $id))
               ->one();

    $joint_entries = $joint
                       ->getJointEntries()
                       ->all();

    echo $this->render($joints_tn, array(
      'params'        => $params,
      'joint'         => $joint,
      'joint_entries' => $joint_entries,
    ));
  }


  /**
  Joint Entries
   */
  public function actionJoint_entries() {
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('id', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Parameter ID required!',
      ));
      return;
    }

    $joint_entries_tn = JointEntry::tableName();

    $id = $params['id'];
    $joint_entry = JointEntry::find()
                     ->where(array('id' => $id))
                     ->one();
 
    $joint = $joint_entry
               ->getJoint()
               ->one();

    $doc = $joint_entry
             ->getDoc()
             ->one();

    $sheet = $doc
               ->getSheet()
               ->one();

    $fileprocessing = $sheet
                        ->getFileProcessing()
                        ->one();

    $file = $fileprocessing
              ->getFile()
              ->one();

    $dir = $file
             ->getDir()
             ->one();

    $handler = $fileprocessing
                 ->getHandler()
                 ->one();

    echo $this->render($joint_entries_tn, array(
      'params'         => $params,
      'dir'            => $dir,
      'file'           => $file,
      'handler'        => $handler,
      'fileprocessing' => $fileprocessing,
      'sheet'          => $sheet,
      'doc'            => $doc,
      'joint'          => $joint,
      'joint_entry'    => $joint_entry,
    ));
  }


}

?>
