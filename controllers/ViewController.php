<?php // Stan 2013-05-24

namespace app\controllers;

use Yii;
use \yii\web\Controller;
use \yii\base\HttpException;

require_once 'lib/meta_array.php';
require_once 'lib/sql.php';
require_once 'lib/view.php';
require_once 'lib/common.php';


class ViewController extends Controller {
  public $layout = '@app/views/layouts/view';


  /**
  Просмотр значений колонок всех таблиц
   */
  public function actionIndex() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    $metaArray = new \MetaArray();

    $tables = array();
  
    $schemas = $connection->schema->tableSchemas;
    foreach( $schemas as $schema ):
      $tablename = $schema->name;
      $tableparams = $metaArray->get($tablename);
  
      // Получаем кол-во записей в таблице
      list($count, $all) = sql_count2($connection, $tablename, '*', $metaArray);

      // Получаем список primaryKey и foreignKeys
      $pfkeys = array();
      if ( $schema->primaryKey )
        $pfkeys[] = $schema->primaryKey[0];
      if ( $schema->foreignKeys )
        foreach ( $schema->foreignKeys as $foreignKey ) {
          $pfkeys[] = array_keys($foreignKey)[1];
        } // foreach
  
      $columns = array();
      foreach ( $schema->columns as $columnname => $column ):
        if ( in_array($column->name, $pfkeys) )
          continue;
  
        if ( $column->dbType == "BLOB" )
          continue;
  
        $columncount = sql_count($connection, $tablename, "DISTINCT(`" . $columnname . "`)", $metaArray);
        $values = get_distinct($connection, $tablename, $column->name, $metaArray);
  
        $columns[$columnname] = array($columncount, $column->type, $values);
      endforeach;
  
      $tables[$tablename] = array($count, $all, $columns);
    endforeach;

    echo $this->render('index', array(
      'params'    => $params,
      'metaArray' => $metaArray,
      'tables'    => $tables,
    ));
  }


  /**
  Страница отладки
   */
  public function actionDebug() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    $metaArray = new \MetaArray();

    echo $this->render('debug', array(
      'params'    => $params,
      'metaArray' => $metaArray,
    ));
  }


  /**
  Просмотр значений заданной колонки
   */
  public function actionColumn() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('table', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Table parameter required!',
      ));
      return;
    }

    if ( !array_key_exists('column', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Column parameter required!',
      ));
      return;
    }

    $table = $params['table'];

    $tables = $connection->schema->tableNames;
    if ( !in_array($table, $tables) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Table is missing!',
      ));
      return;
    }

    $column = $params['column'];

//     $schemas = $connection->schema->tableSchemas;
//     if ( !in_array($column, $schemas[*]['columns']) ) {
//       echo $this->render('/site/error', array(
//         'name'    => 'Error',
//         'message' => 'Column is missing!',
//       ));
//       return;
//     }

    $all = sql_count($connection, $table, "DISTINCT(`" . $column . "`)", NULL);
    $rows = get_distinct($connection, $table, $column, NULL, NULL);

    echo $this->render('column', array(
      'params' => $params,
      'table'  => $table,
      'column' => $column,
      'all'    => $all,
      'rows'   => $rows,
    ));
  }


  /**
  Просмотр заданной таблицы
   */
  public function actionTable() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    if ( !array_key_exists('table', $params) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Table parameter required!',
      ));
      return;
    }

    $table = $params['table'];

    $tables = $connection->schema->tableNames;
    if ( !in_array($table, $tables) ) {
      echo $this->render('/site/error', array(
        'name'    => 'Error',
        'message' => 'Table is missing!',
      ));
      return;
    }

    $metaArray = new \MetaArray();

    list($count, $all) = sql_count2($connection, $table, '*', $metaArray);

    $schema = get_schema($connection, $table);
    $columns = $schema->columns;

    $sortby = array_key_exists('name', $columns) ? 'name' : NULL;
    $rows = sql_select($connection, $table, '*', $metaArray, $sortby);

    echo $this->render('table', array(
      'params' => $params,
      'count'  => $count,
      'all'    => $all,
      'rows'   => $rows,
    ));
  }


  /**
  Просмотр сведений о базе данных
   */
  public function actionDbInfo() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    // Скрываем имя пользователя / пароль
    $connection->username = '*******';
    $connection->password = '*******';

    echo $this->render('dbinfo', array(
      'params'     => $params,
      'connection' => $connection,
    ));
  }


  /**
  Просмотр сведений о таблицах
   */
  public function actionTablesInfo() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    $schemas = $connection->schema->tableSchemas;

    echo $this->render('tablesinfo', array(
      'params'  => $params,
      'schemas' => $schemas,
    ));
  }


}

?>
