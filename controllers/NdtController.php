<?php // Stan 2013-09-20

namespace app\controllers;

use Yii;
use \yii\web\Controller;
use \yii\base\HttpException;

use app\models\Doc;
use app\models\Unit;
use app\models\Entry;
use app\models\Joint;
use app\models\JointEntry;

require_once 'lib/meta_array.php';
require_once 'lib/sql.php';
require_once 'lib/view.php';
require_once 'lib/common.php';


class NdtController extends Controller {
  public $layout = '@app/views/layouts/view';


  /**
  Index
   */
  public function actionIndex() {
    list($route, $params) = Yii::$app->request->resolve();

    echo $this->render('debug', array(
      'params' => $params,
    ));
  }


  /**
  Документы
   */
  public function actionDocs() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    $doc_tn = Doc::tableName();

    $metaArray = new \MetaArray();

    $rows = Doc::find()
              ->where( $metaArray->serialize_sql( $doc_tn ) )
              ->all();

    echo $this->render('docs', array(
      'params' => $params,
      'rows'   => $rows,
    ));
  }


  /**
  Cтыки
   */
  public function actionJoints() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    $doc_tn   = Doc::tableName();
    $joint_tn = Joint::tableName();
    $entry_tn = JointEntry::tableName();

    $metaArray = new \MetaArray();

    $sql = 'SELECT DISTINCT "%2$s".id, "%2$s".name
FROM
  "%3$s"
  INNER JOIN 
  "%1$s"
    ON "%1$s".id = _docs_id,
  "%2$s"
    ON "%2$s".id = _joints_id
WHERE
  %4$s AND %5$s AND %6$s
ORDER BY "%2$s".name
';
    $where4 = $metaArray->serialize_sql( $doc_tn );
    $where5 = $metaArray->serialize_sql( $joint_tn );
    $where6 = $metaArray->serialize_sql( $entry_tn );
    $sql = sprintf( $sql, $doc_tn, $joint_tn, $entry_tn, $where4, $where5, $where6 );
    $command = $connection->createCommand( $sql );
    $joints = $command->queryAll();

    $rows = array();
    foreach ( $joints as $joint ) {
      $id = $joint['id'];
      $name = $joint['name'];

      $sql = 'SELECT "%1$s".*, "%3$s".decision, "%3$s".remarks
FROM
  "%3$s"
  INNER JOIN 
  "%1$s"
    ON "%1$s".id = _docs_id
WHERE
  _joints_id = \'%4$s\'
';
      $sql = sprintf( $sql, $doc_tn, $joint_tn, $entry_tn, $id );
      $command = $connection->createCommand( $sql );
      $docs = $command->queryAll();
 
      $rows2 = array();
      foreach ( $docs as $doc ) {
        $rows2[$doc['type']][] = array($doc['name'], $doc['kind'], $doc['decision'], $doc['remarks']);
      } // foreach

      $rows[] = array($id, $name, $rows2);
    } // foreach
 
    $methods = Doc::find()
                 ->select('type')
                 ->distinct('type')
                 ->orderBy('type')
                 ->all();
 
    echo $this->render('joints', array(
      'params'  => $params,
      'rows'    => $rows,
      'methods' => $methods,
    ));
  }


  /**
  Элементы трубопровода
   */
  public function actionUnits() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    $unit_tn = Unit::tableName();

    $metaArray = new \MetaArray();

    $rows = Unit::find()
              ->where( $metaArray->serialize_sql( $unit_tn ) )
              ->all();

    echo $this->render('units', array(
      'params' => $params,
      'rows'   => $rows,
    ));
  }


  /**
  Журнал НК
   */
  public function actionLog() {
    $connection = Yii::$app->db;
    list($route, $params) = Yii::$app->request->resolve();

    $doc_tn   = Doc::tableName();
    $joint_tn = Joint::tableName();
    $entry_tn = JointEntry::tableName();

    $metaArray = new \MetaArray();

    $sql = 'SELECT "%2$s".name as %2$s_name, "%1$s".name as %1$s_name, "%2$s".*, "%1$s".*, "%3$s".*
FROM
  "%3$s"
  INNER JOIN 
  "%1$s"
    ON "%1$s".id = _docs_id,
  "%2$s"
    ON "%2$s".id = _joints_id
WHERE
  %4$s AND %5$s AND %6$s
ORDER BY "%2$s".joint_pre1, "%2$s".joint_seq DESC, "%2$s".joint_sign1
';
    $where4 = $metaArray->serialize_sql( $doc_tn );
    $where5 = $metaArray->serialize_sql( $joint_tn );
    $where6 = $metaArray->serialize_sql( $entry_tn );
    $sql = sprintf( $sql, $doc_tn, $joint_tn, $entry_tn, $where4, $where5, $where6 );
    $command = $connection->createCommand( $sql );
    $rows = $command->queryAll();

    echo $this->render('log', array(
      'params' => $params,
      'rows'   => $rows,
    ));
  }

}

?>
