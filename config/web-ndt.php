<?php

$params = require(__DIR__ . '/params.php');

$config = [
	'id' => 'basic',
	'basePath' => dirname(__DIR__),
// 	'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
	'components' => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'app\models\User',
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mail' => [
			'class' => 'yii\swiftmailer\Mailer',
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=localhost;dbname=yii2basic',
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		],
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['preload'][] = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';
	$config['modules']['gii'] = 'yii\gii\Module';
}

if (YII_ENV_TEST) {
	// configuration adjustments for 'test' environment.
	// configuration for codeception test environments can be found in codeception folder.

	// if needed, customize $config here.
}


// mine


$config['id'] = 'phpndt';
$config['name'] = 'phpNDT';
$config['language'] = 'ru';
 
$config['components']['db'] = array(
    'class'    => 'yii\db\Connection',
    'dsn'      => 'sqlite:C:\Users\User\12V0304.sqlite',
//     'username' => 'root',
//     'password' => '54321',
    'charset'  => 'utf8',
);


return $config;

/*
      'dsn' => 'mysql:host=localhost;dbname=mydatabase', // MySQL, MariaDB
      'dsn' => 'sqlite:/path/to/database/file', // SQLite
      'dsn' => 'pgsql:host=localhost;port=5432;dbname=mydatabase', // PostgreSQL
      'dsn' => 'sqlsrv:Server=localhost;Database=mydatabase', // MS SQL Server, sqlsrv driver
      'dsn' => 'dblib:host=localhost;dbname=mydatabase', // MS SQL Server, dblib driver
      'dsn' => 'mssql:host=localhost;dbname=mydatabase', // MS SQL Server, mssql driver
      'dsn' => 'oci:dbname=//localhost:1521/mydatabase', // Oracle
*/

?>
